package org.yardstickframework.ampool.region;

import java.util.Map;

import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.Get;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.ftable.FTable;

public class AmpoolGetBenchmark extends AmpoolTableAbstractBenchmark{

	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		int key = nextRandom(args.range());
		mtable.get(new Get(Bytes.toBytes(key)));
		return true;
	}

	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}

}
