package org.yardstickframework.ampool.region;

import static org.yardstickframework.BenchmarkUtils.println;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.yardstickframework.BenchmarkConfiguration;

import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.ftable.FTable;
import io.ampool.monarch.table.ftable.Record;

public class AmpoolSparkSqlBenchmark extends AmpoolSparkAbstractBenchmark{

	private SQLContext sqlContext;
	
	private static final DecimalFormat format = new DecimalFormat("##.##");
	
	public void setUp(BenchmarkConfiguration cfg) throws Exception{
		super.setUp(cfg);
		println(cfg, "Populating query data...");
		
		long start = System.nanoTime();
		
		for (int i = 0; i < args.range() && !Thread.currentThread().isInterrupted(); i++){
			Record record = new Record();
			record.add(columnNamesPerson[0], i);
			record.add(columnNamesPerson[1], 0);
			record.add(columnNamesPerson[2], "firstName"+i);
			record.add(columnNamesPerson[3], "lastName"+i);
			record.add(columnNamesPerson[4], i * 1000.0);
			ftable.append(record);
			
			if (i % 100000 == 0)
                println(cfg, "Populated Employees: " + i);
		}
		
		sqlContext = new SQLContext(sc);
		
		Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(personTable);
	    df.registerTempTable("temp_"+personTable);
	    
		
		println(cfg, "Finished populating query data in " + ((System.nanoTime() - start) / 1_000_000) + " ms.");
	}
	
	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		double salary = ThreadLocalRandom.current().nextDouble() * args.range() * 1000;
		double maxSalary = salary + 1000;
		
		Collection<Row> entries = executeQuery(salary, maxSalary);
		
		for (Row row : entries) {
            double sal = row.getDouble(4);

            if (sal < salary || sal > maxSalary) {
                throw new Exception("Invalid Employee retrieved [min=" + salary + ", max=" + maxSalary +
                    ", EmpId=" + row.getInt(1) + ']');
            }
        }
		return true;
	}
	
	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}
	
	private Collection<Row> executeQuery(double salary,double maxSalary){
		String sql = "SELECT * FROM " + "temp_"+personTable
				+" WHERE SALARY >= " + format.format(salary) + " AND SALARY <= "+ format.format(maxSalary);
		
		return sqlContext.sql(sql).collectAsList();
	}

}
