package org.yardstickframework.ampool.region;

import java.util.Map;

import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.Get;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.Put;
import io.ampool.monarch.table.Row;
import io.ampool.monarch.table.ftable.FTable;

public class AmpoolPutGetBenchmark extends AmpoolTableAbstractBenchmark{

	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		int key = nextRandom(args.range());
		Row row = mtable.get(new Get(Bytes.toBytes(key)));
		if(row != null){
			key = nextRandom(args.range());
		}
		Put put = new Put(Bytes.toBytes(key));
		put.addColumn(Bytes.toBytes("ID"), Bytes.toBytes(key));
		mtable.put(put);
		return true;
	}

	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}

}
